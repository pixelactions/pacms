from django.db import models
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.admin.edit_handlers import (FieldPanel, MultiFieldPanel)
from django.conf import settings
from wagtail.core.models import Orderable


class SocialMediaLinkAbstract(models.Model):
    social_media_class = models.CharField(max_length=100, blank=True)
    social_media_url = models.URLField(max_length=255, blank=True)
    social_media_icon_class = models.CharField(max_length=200, blank=True)
    social_media_icon = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.CASCADE, related_name='+'
    )

    panels = [
        FieldPanel('social_media_class'),
        FieldPanel('social_media_url'),
        FieldPanel('social_media_icon_class'),
        ImageChooserPanel('social_media_icon'),
    ]

    def __str__(self):
        if self.social_media_class:
            return self.social_media_class
        else:
            return "Social Media Link %s" % self.pk

    class Meta:
        verbose_name_plural = 'Social Media Links'
        abstract = True


class MetaTagsAbstract(Orderable):
    schema_title = models.CharField(max_length=150, blank=True)
    schema_description = models.TextField(max_length=150, blank=True)
    schema_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    twitter_title = models.CharField(max_length=150, blank=True)
    twitter_description = models.TextField(max_length=150, blank=True)
    twitter_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    open_graph_title = models.CharField(max_length=150, blank=True)
    open_graph_description = models.TextField(max_length=150, blank=True)
    open_graph_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    panels = [
        MultiFieldPanel([
            FieldPanel('schema_title'),
            FieldPanel('schema_description'),
            ImageChooserPanel('schema_image'),
        ], heading="Google+ (Schema markup) Meta tags",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel([
            FieldPanel('twitter_title'),
            FieldPanel('twitter_description'),
            ImageChooserPanel('twitter_image'),
        ], heading="Twitter Meta tags",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel([
            FieldPanel('open_graph_title'),
            FieldPanel('open_graph_description'),
            ImageChooserPanel('open_graph_image'),
        ], heading="Facebook/Linkedin (Open Graph) Meta tags",
            classname="collapsible collapsed"
        ),
    ]

    class Meta:
        abstract = True
