from django.apps import AppConfig


class AgencyConfig(AppConfig):
    name = 'pacms.agency'
