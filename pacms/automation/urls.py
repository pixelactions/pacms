from django.conf.urls import url
from .views import export, populate_child_pages

app_name = 'helpers'
urlpatterns = [
    url(r'^export/(?P<page_id>\d+)/$', export, name='export'),
    url(r'^populate-child=pages/(?P<page_id>\d+)/$', populate_child_pages, name='populate_child_pages'),
]
