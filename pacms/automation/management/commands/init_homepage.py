from django.core.management.base import BaseCommand
from django.apps import apps

class Command(BaseCommand):
    def handle(self, *args, **options):
        ContentType = apps.get_model('contenttypes.ContentType')
        Page = apps.get_model('wagtailcore.Page')
        Site = apps.get_model('wagtailcore.Site')
        HomePage = apps.get_model('helpers.HomePage')

        Page.objects.filter(id=2).delete()
        Page.objects.filter(slug='home').delete()

        homepage_content_type, __ = ContentType.objects.get_or_create(
            model='homepage', app_label='helpers')

        homepage = HomePage.objects.create(
            title="Home",
            draft_title="Home",
            slug='home',
            content_type=homepage_content_type,
            path='00010001',
            depth=2,
            numchild=0,
            url_path='/home/',
            template_name='index.html',
        )

        # Create a site with the new homepage set as the root
        Site.objects.create(
            hostname='localhost', root_page=homepage, is_default_site=True)
