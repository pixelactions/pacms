import os
from os import listdir
from os.path import isfile, join
from django.core.management.base import BaseCommand
from wagtail.core.models import Collection
import shutil
from pacms.marketing.models import Seo
from django.conf import settings

class Command(BaseCommand):

    def handle(self, *args, **options):

        '''image_app_model = settings.WAGTAILIMAGES_IMAGE_MODEL.replace('models.','')
        image_model, image_app = image_app_model.rsplit('.', 1)
        ImageModel = apps.get_model(image_model, image_app)'''
        if os.path.exists("dev_assets/img/social"):
            try:
                Collection.objects.get(name='Social Media preview images')
                Collection.objects.get(name='Social Media original images')
            except:
                root_coll = Collection.get_first_root_node()
                root_coll.add_child(name='Social Media preview images')
                root_coll.add_child(name='Social Media original images')

            get_social_media_preview_image_collection = Collection.objects.get(name='Social Media preview images')
            get_social_media_original_image_collection = Collection.objects.get(name='Social Media original images')

            settings.WAGTAILIMAGES_IMAGE_MODEL.objects.filter(
                collection=get_social_media_preview_image_collection
            ).delete()

            settings.WAGTAILIMAGES_IMAGE_MODEL.objects.filter(
                collection=get_social_media_original_image_collection
            ).delete()

            original_image_list = [f for f in
                                   listdir(os.path.join((settings.BASE_DIR), ('dev_assets'), ('img'), ('social'))) if
                                   isfile(
                                       join(os.path.join((settings.BASE_DIR), ('dev_assets'), ('img'), ('social')), f))]
            # Copy social images from dev_assets/img/social to media/social
            dst_dir = os.path.join((settings.BASE_DIR), ('media'), ('social'))

            for filename in original_image_list:
                shutil.copy((os.path.join((settings.BASE_DIR), ('dev_assets'), ('img'), ('social'), "%s" % filename)),
                            dst_dir)

            for filename in original_image_list:
                name, extension = filename.split('.')
                social_media_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.create(
                    file=os.path.join((settings.BASE_DIR), ('media'), ('social'), "%s" % filename),
                    title=name,
                    collection=get_social_media_original_image_collection
                )

                ## check if a file exists on disk ##
                ## if exists, delete it else show message on screen ##
                if os.path.join(os.path.join('images'),
                                "%s"".2e16d0ba.fill-300x150.jpegquality-60"".%s" % (name, extension)):
                    try:
                        os.remove(os.path.join(os.path.join(settings.BASE_DIR), ('media'), ('images'),
                                               "%s"".2e16d0ba.fill-300x150.jpegquality-60"".%s" % (name, extension)))
                    except OSError as e:
                        print("Error: %s - %s." % (e.filename, e.strerror))
                else:
                    print("Sorry, I can not find %s file." % filename)

                rendition = social_media_image.get_rendition('fill-300x150|jpegquality-60')

                rendition_path = rendition.file.name.split('.')[0]
                name = rendition_path.split('/')[-1]

                preview_image = rendition.file.name

                social_media_preview_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.create(
                    file=preview_image,
                    title=name + "_preview_image",
                    collection=get_social_media_preview_image_collection
                )

            seo = Seo.objects.first()
            try:
                seo.twitter_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.get(
                    title='twitter_preview_image',
                )
            except:
                seo.twitter_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.get(
                    title='generic_preview_image',
                )
            try:
                seo.schema_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.get(
                    title='google_preview_image',
                )
            except:
                seo.schema_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.get(
                    title='generic_preview_image',
                )
            try:
                seo.open_graph_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.get(
                    title='facebook_preview_image',
                )
            except:
                seo.open_graph_image = settings.WAGTAILIMAGES_IMAGE_MODEL.objects.get(
                    title='generic_preview_image',
                )

            seo.save()
