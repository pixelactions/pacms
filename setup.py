from setuptools import setup

setup(name='pacms',
      version='0.0.13',
      description='A Set of tools, views, management commands and best practices to create amazing projects based on Wagtail CMS',
      url='https://bitbucket.org/pixelactions/pacms',
      author='The PA Team',
      author_email='webmaster@pixelactions.com',
      license='MIT',
      packages=['pacms'],
      install_requires=[],
      include_package_data=True,
      zip_safe=False)