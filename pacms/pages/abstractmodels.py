from django.shortcuts import render, redirect
from django.db import models
from django.http import Http404
from wagtail.images.edit_handlers import ImageChooserPanel
from django.utils.translation import ugettext_lazy
from wagtail.documents.edit_handlers import DocumentChooserPanel
from .mixins import GalleryMixin
from wagtail.core.models import Page, Orderable
from wagtail.admin.edit_handlers import (FieldRowPanel, FieldPanel,
                                         InlinePanel, MultiFieldPanel, ObjectList, TabbedInterface, PageChooserPanel)
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel

from django.conf import settings


'''class SeoFields(models.Model):
    schema_title = models.CharField(max_length=150, blank=True)
    schema_description = models.TextField(max_length=150, blank=True)
    schema_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    twitter_title = models.CharField(max_length=150, blank=True)
    twitter_description = models.TextField(max_length=150, blank=True)
    twitter_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    open_graph_title = models.CharField(max_length=150, blank=True)
    open_graph_description = models.TextField(max_length=150, blank=True)
    open_graph_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    panels = [
        MultiFieldPanel([
            FieldPanel('schema_title'),
            FieldPanel('schema_description'),
            ImageChooserPanel('schema_image'),
        ], heading="Google+ (Schema markup) Meta tags",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel([
            FieldPanel('twitter_title'),
            FieldPanel('twitter_description'),
            ImageChooserPanel('twitter_image'),
        ], heading="Twitter Meta tags",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel([
            FieldPanel('open_graph_title'),
            FieldPanel('open_graph_description'),
            ImageChooserPanel('open_graph_image'),
        ], heading="Facebook/Linkedin (Open Graph) Meta tags",
            classname="collapsible collapsed"
        ),
    ]

    class Meta:
        abstract = True'''


class CustomPageAbstract(Page):
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now_add=True)
    optional_nav_menu_title = models.CharField(max_length=100, null=True, blank=True,
                                               verbose_name="Optional navigation menu title",
                                               help_text="If this field is populated it will be used in the websites navigation menu if skipped the page title will be used.")
    show_in_footer = models.BooleanField(default=False,
                                         help_text="Whether a link to this page will appear in automatically generated footer menu ")
    content_panels = [
        FieldPanel('title', classname="full title"),
        FieldPanel('optional_nav_menu_title', classname="full title"),
        InlinePanel('meta_tags', label="Meta tags"),
    ]
    promote_panels = [
        MultiFieldPanel([
            FieldPanel('slug'),
            FieldPanel('show_in_menus'),
            FieldPanel('show_in_footer'),
            FieldPanel('search_description'),
        ], ugettext_lazy('Common page configuration')),
    ]
    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        #ObjectList(SeoFields.panels, heading='Meta Tags'),
        ObjectList(promote_panels, heading='Promote'),
        ObjectList(Page.settings_panels, heading='Settings', classname="settings"),
    ])

    class Meta:
        abstract = True


class StaticPageAbstract(CustomPageAbstract):
    template_name = models.CharField(max_length=200, blank=True)
    content_panels = CustomPageAbstract.content_panels + [
        FieldPanel('template_name'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        #ObjectList(SeoFields.panels, heading='Meta Tags'),
        ObjectList(CustomPageAbstract.promote_panels, heading='Promote'),
        ObjectList(Page.settings_panels, heading='Settings', classname="settings"),
    ])

    def get_template(self, request):
        return self.template_name

    class Meta:
        abstract = True


class RedirectPageAbstract(CustomPageAbstract):
    external_redirect = models.URLField("External link", blank=True)
    page_redirect = models.ForeignKey(
        'wagtailcore.Page',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.page_redirect:
            return self.page_redirect.url
        else:
            return self.external_redirect

    content_panels = CustomPageAbstract.content_panels + [
        FieldPanel('external_redirect'),
        PageChooserPanel('page_redirect'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        ObjectList(CustomPageAbstract.promote_panels, heading='Promote'),
        ObjectList(Page.settings_panels, heading='Settings', classname="settings"),
    ])

    def serve(self, request, *args, **kwargs):
        if self.link:
            return redirect(self.link, permanent=False)
        raise Http404("Redirection not defined")

    class Meta:
        abstract = True


class ContactPageAbstract(AbstractEmailForm):
    template_name = models.CharField(max_length=100, blank=True, null=True)
    subpage_types = []

    # def contact_forms(self):
    #     contact_forms = Page.objects.type(AbstractEmailForm)
    #     if contact_forms:
    #         return contact_forms
    #     else:
    #         return None

    def get_template(self, request):
        if self.template_name:
            return self.template_name
        else:
            return super(ContactPageAbstract, self).get_template(request)

    def get_landing_page_template(self, request):
        if self.template_name:
            return self.template_name
        else:
            return super(ContactPageAbstract, self).get_landing_page_template(request)

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('template_name'),
        InlinePanel('meta_tags', label="Meta tags"),
        FormSubmissionsPanel(),
        InlinePanel('form_fields', label="Form fields"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    class Meta:
        abstract = True


# TODO - Decide it's fate
class ListViewPageAbstract(StaticPageAbstract):
    child_pages_csv = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Upload a csv file with the correct format style.Use the download link below to generate a csv file from the child pages allready in the database.Follow the same format to add more.Upload the file and publish and the entries will autopopulate the database.'
    )

    content_panels = StaticPageAbstract.content_panels + [
        DocumentChooserPanel('child_pages_csv'),
    ]
    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        #ObjectList(SeoFields.panels, heading='Meta Tags'),
        ObjectList(CustomPageAbstract.promote_panels, heading='Promote'),
        ObjectList(Page.settings_panels, heading='Settings', classname="settings"),
    ])

    def serve(self, request, *args, **kwargs):
        context = self.get_context(request)
        child_objects = self.get_children()
        context['objects'] = child_objects
        if self.template:
            return render(
                request,
                self.template,
                context
            )
        else:
            return redirect('/', permanent=False)

    class Meta:
        abstract = True


# TODO - Decide it's fate
class DetailViewPageAbstract(CustomPageAbstract, GalleryMixin):
    description = models.TextField(blank=True)
    featured_on_homepage = models.BooleanField(default=False)
    logo = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )

    featured_image = models.ForeignKey(
        settings.WAGTAILIMAGES_IMAGE_MODEL, blank=True, null=True, on_delete=models.SET_NULL,
        related_name='+'
    )

    facebook_url = models.URLField(blank=True)
    pinterest_url = models.URLField(blank=True)
    google_plus_url = models.URLField(blank=True)
    youtube_url = models.URLField(blank=True)
    instagram_url = models.URLField(blank=True)

    order = models.IntegerField(default=1)
    subpage_types = []

    content_panels = CustomPageAbstract.content_panels + GalleryMixin.content_panels + [
        FieldPanel('description'),
        FieldPanel('featured_on_homepage'),
        ImageChooserPanel('logo'),
        ImageChooserPanel('featured_image'),
        FieldPanel('facebook_url'),
        FieldPanel('pinterest_url'),
        FieldPanel('google_plus_url'),
        FieldPanel('youtube_url'),
        FieldPanel('instagram_url'),
        FieldPanel('order'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        #ObjectList(SeoFields.panels, heading='Meta Tags'),
        ObjectList(CustomPageAbstract.promote_panels, heading='Promote'),
        ObjectList(Page.settings_panels, heading='Settings', classname="settings"),
    ])

    def serve(self, request, *args, **kwargs):
        context = self.get_context(request)
        return render(
            request,
            'detail-view.html',
            context
        )

    class Meta:
        abstract = True
