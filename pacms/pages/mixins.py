from wagtail.admin.edit_handlers import FieldPanel
from django.db import models
from wagtail.core.models import Collection, get_root_collection_id
from wagtail.images import get_image_model



class GalleryMixin(models.Model):
    collection = models.ForeignKey(
        Collection,

        default=get_root_collection_id,
        verbose_name='collection',
        related_name='+',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    show_gallery = models.BooleanField(default=False)

    content_panels = [
        FieldPanel('collection'),
        FieldPanel('show_gallery'),
        # FieldPanel('image', widget=forms.CheckboxSelectMultiple),
    ]

    def image_collection(self):
        image_model = get_image_model()
        collection_images = image_model.objects.filter(collection__name=self.collection).order_by('order')
        return collection_images

    class Meta:
        abstract = True