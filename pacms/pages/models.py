from django.db import models
from wagtail.images.models import Image, AbstractImage, AbstractRendition, get_upload_to
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import os


class OrderableImage(AbstractImage):
    # Add any extra fields to image here
    order = models.IntegerField(default=1, blank=True, null=True)
    # eg. To add a caption field:
    caption = models.CharField(max_length=255, blank=True)
    file = models.ImageField(
        verbose_name=_('file'), upload_to=get_upload_to, max_length=500, width_field='width', height_field='height'
    )
    admin_form_fields = Image.admin_form_fields + (

        # Then add the field names here to make them appear in the form:
        'caption',
        'order',
    )


class OrderableImageRendition(AbstractRendition):
    image = models.ForeignKey(OrderableImage, on_delete=models.CASCADE, related_name='renditions')

    class Meta:
        unique_together = (
            ('image', 'filter_spec', 'focal_point_key'),
        )

    def get_upload_to(self, filename):
        if hasattr(settings, 'PACMS_RENDITION_FOLDER'):
            rendition_folder = settings.PACMS_RENDITION_FOLDER
            folder_name = os.path.join('images', rendition_folder)
        else:
            folder_name = os.path.join('images')
        filename = self.file.field.storage.get_valid_name(filename)
        return os.path.join(folder_name, filename)

