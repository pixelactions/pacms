$(function () {
    // Rich text
    var rich_text_areas = $('.rich_text_area');
    rich_text_areas.each(function () {
        var specificRichText = $(this)
        var parentElement = specificRichText.parent().parent().parent().parent();
        var helpBox = parentElement.find('.object-help');
        if (helpBox.html()) {
            helpBox.addClass('charcount');
        } else {
            var helpBoxExists = true
            var elem = parentElement.append("<div style='opacity:1;' class='object-help help charcount'></div>");
            var helpBox = parentElement.find('.charcount');
            helpBox.hide();
        }
        var whiteSpace = /\s\s+/gm;
        var wordsRegex = /\s+/gi;
        var charCountElemText = helpBox.text();
        var maxChars = specificRichText.find('textarea').attr('maxlength');

        $(specificRichText.find('.richtext')).bind('hallomodified', function (event) {
            var text = event.currentTarget.innerText;
            var textNoWhitespace = text.replace(whiteSpace, ' ');
            var textWordCount = textNoWhitespace.trim().replace(wordsRegex, ' ').split(' ').length;
            var textCharCount = textNoWhitespace.length - 1;
            var backendCount = $(specificRichText).find('.richtext').html().length;
            if (charCountElemText.length > 1) {
                helpBox.css({opacity: 1});
                helpBox.html(
                    charCountElemText + '<br>' +
                    "<br>" + textWordCount + " words" +
                    "<br><span class='count'>" + backendCount + ' / ' + maxChars + '</span> characters');
                if (backendCount > maxChars) {
                    $(helpBox.find('.count')).css({color: 'red'})
                } else if (backendCount + 20 > maxChars) {
                    $(helpBox.find('.count')).css({color: 'orange'})
                }
            } else {
                helpBox.show();
                helpBox.html(
                    textWordCount + " words" +
                    "<br><span class='count'>" + backendCount + ' / ' + maxChars + '</span> characters');
                if (backendCount > maxChars) {
                    $(helpBox.find('.count')).css({color: 'red'})
                } else if (backendCount + 20 > maxChars) {
                    $(helpBox.find('.count')).css({color: 'orange'})
                }
            }
        });
    })
    // Normal text fields
    var text_fields = $($('form').find('.field.char_field').not('.rich_text_area'));
    text_fields.each(function () {
        var specificTextBox = $(this);
        var parentElement = specificTextBox.parent().parent().parent().parent();
        if (specificTextBox.find('input').attr('maxlength')) {
            var helpBox = parentElement.find('.charfield_input');
        } else {
            var helpBox = parentElement.find('.textarea_input');
        }
        var helpBox = parentElement.find('.object-help');
        if (helpBox.html()) {
            helpBox.addClass('charcount');
        } else {
            var helpBoxExists = true;
            if (specificTextBox.find('input').attr('maxlength')) {
                var child_div = parentElement.append("<div style='opacity:1;' class='object-help charfield_input help charcount'></div>");
            } else {
                var child_div = parentElement.append("<div style='opacity:1;' class='object-help textarea_input help charcount'></div>");
            }
            var helpBox = parentElement.find('.charcount');
            helpBox.hide();
        }
        var whiteSpace = /\s\s+/gm;
        var wordsRegex = /\s+/gi;
        var charCountElemText = helpBox.text();

        if (specificTextBox.find('input').attr('maxlength')) {
            var maxChars = specificTextBox.find('input').attr('maxlength');
        } else {
            var maxChars = specificTextBox.find('textarea').attr('maxlength');
        }

        var add_class_to_input = specificTextBox.find('input').addClass('char_text_fields');
        var add_class_to_textarea = specificTextBox.find('textarea').addClass('char_text_fields');

        $(specificTextBox.find('.char_text_fields')).bind('input propertychange', function () {
            var text = this.value;
            var textNoWhitespace = text.replace(whiteSpace, ' ');
            var textCharCount = textNoWhitespace.length;
            if (textCharCount == 0) {
                var textWordCount = 0
            } else {
                var textWordCount = textNoWhitespace.trim().replace(wordsRegex, ' ').split(' ').length;
            }
            if (charCountElemText.length > 1) {
                helpBox.css({opacity: 1});
                var helpBoxContent = (charCountElemText + '<br>' +
                    "<br>" + textWordCount + " words" +
                    "<br><span class='count'>" + textCharCount + ' / ' + maxChars + '</span> characters');

                helpBox.html(helpBoxContent);
                if (textCharCount > maxChars) {
                    $(helpBox.find('.count')).css({color: 'red'})
                } else if (textCharCount + 20 > maxChars) {
                    $(helpBox.find('.count')).css({color: 'orange'})
                }
            } else {
                var textarea_input = specificTextBox.find('textarea');
                var input = specificTextBox.find('input');
                textarea_input.bind('input propertychange',function () {
                    helpBox.show();
                    $('.charfield_input').hide();
                });
                input.bind('input propertychange',function () {
                    helpBox.show();
                    $('.textarea_input').hide();
                });
                helpBox.html(
                    textWordCount + " words" +
                    "<br><span class='count'>" + textCharCount + ' / ' + maxChars + '</span> characters'
                );
                if (textCharCount > maxChars) {
                    $(helpBox.find('.count')).css({color: 'red'})
                } else if (textCharCount + 20 > maxChars) {
                    $(helpBox.find('.count')).css({color: 'orange'})
                }
            }
        });
    })


});