from __future__ import unicode_literals
from django.shortcuts import reverse
from wagtail.core import hooks
import os
from django.conf import settings
from wagtail.admin import widgets as wagtailadmin_widgets
import codecs
import shutil

@hooks.register('register_page_listing_buttons')
def page_custom_listing_buttons(page, page_perms, is_parent=False):
    yield wagtailadmin_widgets.ButtonWithDropdownFromHook(
        'Download csv - Populate child pages',
        hook_name='my_button_dropdown_hook',
        page=page,
        page_perms=page_perms,
        is_parent=is_parent,
        priority=50
    )


@hooks.register('my_button_dropdown_hook')
def page_custom_listing_more_buttons(page, page_perms, is_parent=False):
    if page.__class__.__name__ == 'ListViewPage':
        yield wagtailadmin_widgets.Button('Download csv', reverse('automation:export', kwargs={'page_id': page.id}),
                                          priority=10)
        if page.child_pages_csv and settings.SERVER_TYPE == 'PROD':
            yield wagtailadmin_widgets.Button('Populate child pages',
                                              reverse('automation:populate_child_pages', kwargs={'page_id': page.id}),
                                              priority=10)
        if page.child_pages_csv and not settings.SERVER_TYPE == 'PROD':
            yield wagtailadmin_widgets.Button('Populate child pages dev',
                                              reverse('automation:populate_child_pages', kwargs={'page_id': page.id}),
                                              priority=10)


@hooks.register('after_create_page')
def do_after_page_create(request, page):
    if hasattr(page,'template_name'):
        try:
            html = codecs.open(os.path.join(settings.BASE_DIR, 'templates', page.template_name), 'r')
            print('html file already exists')
        except:
            def create_html_file():
                src_file = os.path.join(settings.BASE_DIR, 'templates','helpers', 'static_template', 'static.html')
                new_file = os.path.join(settings.BASE_DIR, 'templates', page.template_name)
                shutil.copy(src_file, new_file)
                print('html file missing it will be created')
            created=create_html_file()

@hooks.register('after_edit_page')
def after_edit_page(request, page):
    if hasattr(page,'template_name'):
        try:
            html = codecs.open(os.path.join(settings.BASE_DIR, 'templates', page.template_name), 'r')
            print('html file already exists')
        except:
            def create_html_file():
                src_file = os.path.join(settings.BASE_DIR, 'templates','helpers', 'static_template', 'static.html')
                new_file = os.path.join(settings.BASE_DIR, 'templates', page.template_name)
                shutil.copy(src_file, new_file)
                print('html file missing it will be created')
            created=create_html_file()