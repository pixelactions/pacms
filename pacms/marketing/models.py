from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalKey
from wagtail.contrib.settings.models import BaseSetting, register_setting
from pacms.marketing.abstractmodels import SocialMediaLinkAbstract
from django.db import models
from wagtail.admin.edit_handlers import (InlinePanel, MultiFieldPanel)
from wagtail.core.models import Page
from .abstractmodels import MetaTagsAbstract


@register_setting
class MarketingSettings(ClusterableModel, BaseSetting):
    panels = [
        MultiFieldPanel([
            InlinePanel('social_media_links', label="Social media links")
        ], heading="Social media links",
            classname="collapsible collapsed"
        ),
    ]

    class Meta:
        verbose_name = 'Marketing Settings'


class SocialMediaLink(SocialMediaLinkAbstract):
    settings_model = ParentalKey(MarketingSettings, null=True, on_delete=models.SET_NULL,
                                 related_name='social_media_links')


class MetaTags(MetaTagsAbstract):
    page = ParentalKey(Page, on_delete=models.CASCADE, related_name='meta_tags')


@register_setting
class Seo(MetaTagsAbstract, BaseSetting):
    class Meta:
        verbose_name = 'SEO Global Settings'
