# Generated by Django 2.0.3 on 2019-02-06 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20190131_1044'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderableimage',
            name='caption',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
