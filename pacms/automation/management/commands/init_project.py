from django.core.management.base import BaseCommand
from pacms.automation.management.commands.init_db import Command as init_db
from pacms.automation.management.commands.init_social_media_images import Command as init_social_media_images
from pacms.automation.management.commands.init_template_pack import  Command as init_template_pack
from pacms.automation.management.commands.init_homepage import  Command as init_homepage

class Command(BaseCommand):
    def handle(self, *args, **options):
        user_input = input('Do you want to create a homepage (y/Y): ')
        if user_input in ['yes', 'YES', 'Y', 'y']:
            class_instance = init_homepage()
            class_instance.handle()
        user_input = input('Do you want to import a template pack (y/Y): ')
        if user_input in ['yes', 'YES', 'Y', 'y']:
            class_instance = init_template_pack()
            class_instance.handle()
        user_input = input('Do you want to populate the database (y/Y): ')
        if user_input in ['yes', 'YES', 'Y', 'y']:
            class_instance = init_db()
            class_instance.handle()
        user_input = input('Do you want to create social media preview images (y/Y): ')
        if user_input in ['yes', 'YES', 'Y', 'y']:
            class_instance = init_social_media_images()
            class_instance.handle()
        '''user_input = input('Do you want to create contact form fields (y/Y): ')
        if user_input in ['yes', 'YES', 'Y', 'y']:
            class_instance = init_contact_form()
            class_instance.handle()'''
