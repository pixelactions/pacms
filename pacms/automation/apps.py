from django.apps import AppConfig


class AutomationConfig(AppConfig):
    name = 'pacms.automation'
