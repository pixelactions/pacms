from django.core.management.base import BaseCommand
from wagtail.core.models import Page
from django.apps import apps

class Command(BaseCommand):

    def handle(self, *args, **options):
        seo = 'pages.Seo'
        seo_page_to_be_created = apps.get_model(seo)
        homepage=apps.get_model('helpers','HomePage')
        queryset = Page.objects.exact_type(homepage).specific()
        for parent in queryset:
            for child in parent.subpage_types:
                ContentType = apps.get_model('contenttypes.ContentType')
                page_to_be_created = apps.get_model(child)
                page_label, page_model = child.split('.')
                content_type, created = ContentType.objects.get_or_create(
                    model=page_model.lower(), app_label=page_label.lower())

                child_page = page_to_be_created(
                    title=page_model,
                    live=True,
                    show_in_menus=True,
                    content_type=content_type,
                )
                if not Page.objects.filter(content_type=child_page.content_type):
                    parent.add_child(instance=child_page)
                    self.stdout.write(self.style.SUCCESS('Successfully created %s page ' % page_model))
                else:
                    self.stdout.write(self.style.SUCCESS('Page %s already in database' % page_model))
