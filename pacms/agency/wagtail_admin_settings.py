from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from django.db import models
from modelcluster.models import ClusterableModel
from wagtail.contrib.settings.models import BaseSetting, register_setting


@register_setting
class GeneralSettings(ClusterableModel, BaseSetting):
    default_contact_mail = models.CharField(max_length=100, null=True, blank=True)
    panels = [
        MultiFieldPanel([
            FieldPanel('default_contact_mail')
        ], heading="Contact email",
            classname="collapsible collapsed"
        )
    ]

    class Meta:
        verbose_name = 'General Site Settings'

