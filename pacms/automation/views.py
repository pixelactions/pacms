from __future__ import unicode_literals
from django.http import HttpResponse
from django.utils.encoding import smart_str
import csv
from django.apps import apps
import os
from django.conf import settings
from django.db import models
from wagtail.images import get_image_model
import uuid
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render

from wagtail.core.models import Page
from wagtail.search.models import Query


@user_passes_test(lambda u: u.is_superuser)
def export(request, **kwargs):
    class_name = apps.get_model(app_label='helpers', model_name='ListViewPage')
    page = class_name.objects.get(id=kwargs['page_id'])
    for child in page.subpage_types:
        EXCLUDED_FIELDS = [
            'formsubmission',
            'redirect',
            'sites_rooted_here',
            'revisions',
            'group_permissions',
            'view_restrictions',
            'mainmenuitem',
            'flatmenuitem',
            'id',
            'path',
            'depth',
            'numchild',
            'draft_title',
            'slug',
            'content_type',
            'has_unpublished_changes',
            'url_path',
            'owner',
            'seo_title',
            'show_in_menus',
            'search_description',
            'go_live_at',
            'expire_at',
            'expired',
            'locked',
            'first_published_at',
            'last_published_at',
            'latest_revision_created_at',
            'live_revision',
            'page_ptr',
            'schema_title',
            'schema_description',
            'schema_image',
            'twitter_title',
            'twitter_description',
            'twitter_image',
            'open_graph_title',
            'open_graph_description',
            'open_graph_image',
            'created_on',
            'modified_on',
            'optional_nav_menu_title',
            'show_in_footer',
            'productlines',
            'custompage_ptr',
            'collection'
        ]
        page_label, page_model = child.split('.')
        Model_class = apps.get_model(app_label=page_label, model_name=page_model)
        data_headings = []
        model_objects = Model_class.objects.all()
        print(model_objects)
        for field in Model_class._meta.get_fields():
            if not field.name in EXCLUDED_FIELDS:
                data_headings.append(field.name)

        # Get query parameters. If the user has filtered the list view.

        # return a CSV instead
        response = HttpResponse(content_type='text/csv; charset=utf-8')
        response['Content-Disposition'] = 'attachment;filename=' + \
                                          'child_pages.csv'

        # Prevents UnicodeEncodeError for labels with non-ansi symbols
        data_headings = [smart_str(label) for label in data_headings]

        writer = csv.writer(response)
        writer.writerow(data_headings)

        for reg in model_objects:
            data_row = []
            for field in data_headings:
                field_value = getattr(reg, field)
                if isinstance(field_value, models.Manager):
                    value = field_value.all()
                    field_value = value.values_list('title', flat=True)
                    data_row.append(field_value)
                else:
                    data_row.append(field_value)
            writer.writerow(data_row)
        return response


@user_passes_test(lambda u: u.is_superuser)
def populate_child_pages(request, **kwargs):
    class_name = apps.get_model(app_label='helpers', model_name='ListViewPage')
    page = class_name.objects.get(id=kwargs['page_id'])

    if page.child_pages_csv:
        for child in page.subpage_types:
            csv_url = str(page.child_pages_csv.file)
            file_full_path = os.path.join(settings.MEDIA_ROOT, csv_url)
            with open(file_full_path, encoding="utf8") as child_pages_csv:
                csv_reader = csv.DictReader(child_pages_csv)
                line_count = 0
                ContentType = apps.get_model('contenttypes.ContentType')
                page_to_be_created = apps.get_model(child)
                page_label, page_model = child.split('.')
                content_type, created = ContentType.objects.get_or_create(
                    model=page_model.lower(), app_label=page_label.lower())
                for row in csv_reader:
                    if not settings.SERVER_TYPE == 'PROD':
                        try:
                            number_of_pages_to_create = input('how many child pages to create?')
                            int(number_of_pages_to_create)
                        except:
                            return HttpResponse('Pls try again with correct input(integer).')
                        number_of_pages_to_create = number_of_pages_to_create
                        count = 0
                        while count < int(number_of_pages_to_create):
                            child_page = page_to_be_created(title='%s%s' % (uuid.uuid4(), row['title']))
                            child_page.content_type = content_type
                            for key, value in row.items():
                                try:
                                    if value.split('.')[-1] in ['png', 'jpg', 'jpeg']:
                                        image_model = get_image_model()
                                        image_instance = image_model.objects.get(title=value)
                                        field_value = setattr(child_page, key, image_instance)
                                    else:
                                        try:
                                            if not key == 'title':
                                                field_value = setattr(child_page, key, value)
                                        except Exception as e:
                                            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                                            message = template.format(type(e).__name__, e.args)
                                            field_error_key = key
                                            field_error_value = value
                                except:
                                    continue
                            class_model = apps.get_model(app_label=page_label, model_name=page_model)
                            if not class_model.objects.filter(title=child_page.title):
                                page.add_child(instance=child_page)
                                # child_page.regions.add(region)
                                if field_error_key:
                                    try:
                                        setattr(child_page, field_error_key, field_error_value)
                                    except:
                                        pass
                                print('Successfully created %s page ' % page_model)
                            else:
                                print('Page %s already in database' % page_model)
                            count += 1
                        return HttpResponse('Db populated successfully.')
                    else:
                        child_page = page_to_be_created(title=row['title'])
                        child_page.content_type = content_type
                        for key, value in row.items():
                            try:
                                if value.split('.')[-1] in ['png', 'jpg', 'jpeg']:
                                    image_model = get_image_model()
                                    image_instance = image_model.objects.get(title=value)
                                    field_value = setattr(child_page, key, image_instance)
                                else:
                                    try:
                                        field_value = setattr(child_page, key, value)
                                    except Exception as e:
                                        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                                        message = template.format(type(e).__name__, e.args)
                                        field_error_key = key
                                        field_error_value = value
                            except:
                                continue
                        class_model = apps.get_model(app_label=page_label, model_name=page_model)
                        if not class_model.objects.filter(title=child_page.title):
                            page.add_child(instance=child_page)
                            # child_page.regions.add(region)
                            if field_error_key:
                                try:
                                    setattr(child_page, field_error_key, field_error_value)
                                except:
                                    pass
                            print('Successfully created %s page ' % page_model)
                        else:
                            print('Page %s already in database' % page_model)
                        line_count += 1
                return HttpResponse('Db populated successfully.')


def search(request):
    search_query = request.GET.get('query', None)
    page = request.GET.get('page', 1)

    # Search
    if search_query:
        search_results = Page.objects.live().search(search_query)
        query = Query.get(search_query)

        # Record hit
        query.add_hit()
    else:
        search_results = Page.objects.none()

    # Pagination
    paginator = Paginator(search_results, 10)
    try:
        search_results = paginator.page(page)
    except PageNotAnInteger:
        search_results = paginator.page(1)
    except EmptyPage:
        search_results = paginator.page(paginator.num_pages)

    return render(request, 'search/search.html', {
        'search_query': search_query,
        'search_results': search_results,
    })
