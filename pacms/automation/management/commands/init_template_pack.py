from django.core.management.base import BaseCommand
from django.apps import apps
import zipfile
from django.conf import settings
import os
import json

class Command(BaseCommand):
    def handle(self, *args, **options):
        ContentType = apps.get_model('contenttypes.ContentType')
        Page = apps.get_model('wagtailcore.Page')
        upload_path = os.path.join(settings.BASE_DIR, 'media', 'documents', 'template_pack.zip')
        zip = zipfile.ZipFile(upload_path, 'r')
        for file in zip.infolist():
            if 'cms_configuration/' in file.filename:
                copy_path = os.path.join(settings.BASE_DIR)
                zip.extract(file, path=copy_path)
                continue

        for file in zip.infolist():
            if file.filename == 'template_pack/':
                continue
            if 'dev_assets/' in file.filename:
                copy_path = os.path.join(settings.BASE_DIR)
                zip.extract(file, path=copy_path)
                continue
            else:
                try:
                    var1, var2 = file.filename.split('.')
                except:
                    continue
            unwanted, extension = file.filename.split('.')
            if extension == 'html':
                try:
                    folder, filename = file.filename.split('/')
                    filename, ext = filename.split('.')
                except:
                    filename, ext = file.filename.split('.')
                copy_path = os.path.join(settings.BASE_DIR)
                zip.extract(file, path=copy_path)
                json_data = open('cms_configuration/pages_to_be_created.json')
                data = json.load(json_data)
                parent = apps.get_model(data['parent_page'])
                parent_page = Page.objects.exact_type(parent).specific().get()
                page = data['page_type']
                page_to_be_created = apps.get_model(page)
                page_label, page_model = page.split('.')
                content_type, created = ContentType.objects.get_or_create(
                    model=page_model.lower(), app_label=page_label.lower())

                child_page = page_to_be_created(
                    title=filename,
                    live=True,
                    show_in_menus=True,
                    content_type=content_type,
                    template_name='%s' % file.filename,
                )
                if not Page.objects.filter(title=child_page.title):
                    parent_page.add_child(instance=child_page)
                    self.stdout.write(self.style.SUCCESS('Successfully created %s page ' % filename))
                else:
                    self.stdout.write(self.style.SUCCESS('Page %s already in database' % filename))
            else:
                continue
