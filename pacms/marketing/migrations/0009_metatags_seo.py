# Generated by Django 2.0.7 on 2018-12-05 10:08

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('pages', '0001_initial'),
        ('marketing', '0008_auto_20181205_1208'),
    ]

    operations = [
        migrations.CreateModel(
            name='MetaTags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('schema_title', models.CharField(blank=True, max_length=150)),
                ('schema_description', models.TextField(blank=True, max_length=150)),
                ('twitter_title', models.CharField(blank=True, max_length=150)),
                ('twitter_description', models.TextField(blank=True, max_length=150)),
                ('open_graph_title', models.CharField(blank=True, max_length=150)),
                ('open_graph_description', models.TextField(blank=True, max_length=150)),
                ('open_graph_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.OrderableImage')),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='meta_tags', to='wagtailcore.Page')),
                ('schema_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.OrderableImage')),
                ('twitter_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.OrderableImage')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Seo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('schema_title', models.CharField(blank=True, max_length=150)),
                ('schema_description', models.TextField(blank=True, max_length=150)),
                ('twitter_title', models.CharField(blank=True, max_length=150)),
                ('twitter_description', models.TextField(blank=True, max_length=150)),
                ('open_graph_title', models.CharField(blank=True, max_length=150)),
                ('open_graph_description', models.TextField(blank=True, max_length=150)),
                ('open_graph_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.OrderableImage')),
                ('schema_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.OrderableImage')),
                ('site', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='wagtailcore.Site')),
                ('twitter_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.OrderableImage')),
            ],
            options={
                'verbose_name': 'SEO Global Settings',
            },
        ),
    ]
