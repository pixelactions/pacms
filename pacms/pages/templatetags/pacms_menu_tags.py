from wagtail.core.models import Page
from django.template import Library
from django.apps import apps
from django.contrib.contenttypes.models import ContentType

register = Library()


@register.simple_tag()
def menu_items(inclusive=True):
    home_page = Page.objects.get(slug='home')
    return Page.objects.in_menu().live().child_of(home_page).specific()


@register.simple_tag()
def menu_item_child(parent):
    return Page.objects.child_of(parent).live().in_menu().specific()


@register.simple_tag()
def footer_menu_items(inclusive=True):
    footer_items = []
    home_page = Page.objects.get(slug='home')
    child_pages = home_page.get_children().specific()
    for child in child_pages:
        if child.show_in_footer:
            footer_items.append(child)
    return footer_items


@register.simple_tag()
def page_class_url(page_class_name):
    page_content_type = ContentType.objects.get(model=page_class_name)
    page_class = page_content_type.model_class()
    page_object = page_class.objects.all().first()
    page_full_url = page_object.get_url()
    return page_full_url
