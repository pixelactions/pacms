from django.template import Library
from pacms.marketing.models import Seo, MetaTags
from django.contrib.sites.shortcuts import get_current_site
from django.utils.functional import SimpleLazyObject
from django.apps import apps
from pacms.marketing.abstractmodels import MetaTagsAbstract

SocialMediaLink = apps.get_model('marketing', 'SocialMediaLink')

register = Library()


@register.inclusion_tag('pacms/seo/seo.html', takes_context=True)
def default_seo_settings(context, object):
    try:
        request = context['request']
        values = {}
        try:
            site = object.get_site()
            seo = Seo.objects.get(site=site)
        except Seo.DoesNotExist as e:
            print(e)
            return None
        try:
            meta_tags = MetaTags.objects.get(page=object)
        except MetaTags.DoesNotExist:
            meta_tags = MetaTags()
        for field in MetaTagsAbstract._meta.get_fields():
            if hasattr(meta_tags, field.name):
                if getattr(meta_tags, field.name):
                    values[field.name] = getattr(meta_tags, field.name)
                else:
                    values[field.name] = getattr(seo, field.name)
            else:
                values[field.name] = getattr(seo, field.name)

    except Seo.DoesNotExist:
        values = None
    return {
        'seo': values,
        'request': request,
        'site': site.root_url,
        'page': object
    }


@register.inclusion_tag('pacms/marketing/social_media.html', takes_context=True)
def get_social_media_html(context):
    request = context['request']
    site = SimpleLazyObject(lambda: get_current_site(request))
    social_media = SocialMediaLink.objects.all()
    return {
        'request': request,
        'site': site,
        'social_media': social_media
    }


@register.simple_tag()
def get_social_media_objects():
    return SocialMediaLink.objects.all()
