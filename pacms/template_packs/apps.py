from django.apps import AppConfig


class TemplatePacksConfig(AppConfig):
    name = 'pacms.template_packs'
