from django.conf.urls import url

from wagtail.documents.views import serve
from .views import serve as custom_serve
from wagtail.documents.views.serve import authenticate_with_password
urlpatterns = [
    url(r'^(\d+)/(.*)$', custom_serve, name='wagtaildocs_serve'),
    url(r'^authenticate_with_password/(\d+)/$', authenticate_with_password,
        name='wagtaildocs_authenticate_with_password'),
]
