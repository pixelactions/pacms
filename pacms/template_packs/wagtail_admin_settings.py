from wagtail.contrib.settings.models import BaseSetting, register_setting
from django.db import models
from wagtail.documents.edit_handlers import DocumentChooserPanel
from pacms.automation.management.commands.init_template_pack import Command as init_template_pack


@register_setting
class TemplatePack(BaseSetting):
    zip_file = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Upload the template pack that will be used to populate the website.'
    )

    panels =[
        DocumentChooserPanel('zip_file'),
    ]

    def save(self, *args, **kwargs):
        super(TemplatePack,self).save()
        if getattr(self, 'zip_file', True):
            class_instance = init_template_pack()
            class_instance.handle()


    class Meta:
        verbose_name = 'Template Pack'
