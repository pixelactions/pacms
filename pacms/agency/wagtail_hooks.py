from __future__ import unicode_literals
from django.shortcuts import reverse
from django.utils.html import format_html, format_html_join
from wagtail.core import hooks
from django.conf import settings
from django.conf.urls import url
from django.shortcuts import render
from wagtail.admin.menu import MenuItem


# shows how many characters are left for charfields and textfields in wagtail admin
@hooks.register('insert_editor_js')
def editor_js():
    js_files = ['charcount.js']
    js_includes = format_html_join(
        '\n',
        '<script src="{0}{1}"></script>',
        ((settings.STATIC_URL, filename) for filename in js_files))

    return js_includes + format_html("""
        <script></script>""")


def help_request(request):
    context = {}
    context['DEFAULT_TICKET_DESK'] = settings.DEFAULT_TICKET_DESK
    return render(request, 'helpers/help_request.html', context)


def support(request):
    return render(request, 'support.html')


@hooks.register('register_admin_urls')
def urlconf_time():
    return [
        url(r'^helpdesk/$', help_request, name='helpdesk'),
        url(r'^support/$', support, name='support'),
    ]


@hooks.register('register_admin_menu_item')
def register_helpdeskk_menu_item():
    return MenuItem('Help Request', 'https://pixelactions.freshdesk.com/support/login',attrs={"target":"_blank",},  classnames='icon icon-help', order=10000)


@hooks.register('register_settings_menu_item')
def register_contact_admin_menu_item():
    return MenuItem('Contact an Admin', 'mailto:''%s' % settings.DEFAULT_ADMIN_MAIL, classnames='icon icon-help',
                    order=10000)


@hooks.register('register_settings_menu_item')
def register_pixelactions_menu_item():
    return MenuItem('Pixelactions', "https://www.pixelactions.com",attrs={"target":"_blank",}, classnames='icon icon-help', order=10000)
