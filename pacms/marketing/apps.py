from django.apps import AppConfig


class MarketingConfig(AppConfig):
    name = 'pacms.marketing'
